<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Pagination\LengthAwarePaginator;

class NoticiaController extends Controller
{

    public function index(Request $request)
    {
        $client = new Client();
        $apiKey = '2f49a59ab1cb4c46bdbeaf2a1c5e6db5';
        $page = isset($request->page) ? $request->page : 1;

        $api = "https://newsapi.org/v2/top-headlines?country=us&pageSize=10&page={$page}&apiKey={$apiKey}";
        $response = $client->get($api);
        $noticias = json_decode($response->getBody())->articles;
        $totalResults = json_decode($response->getBody())->totalResults;

    // Iterar sobre las noticias y asignar autores aleatorios
        foreach ($noticias as $key => $noticia) {
            $randomUserResponse = $client->get('https://randomuser.me/api/');
            $randomUserData = json_decode($randomUserResponse->getBody(), true);
            $randomAuthor = $randomUserData['results'][0]['name']['first'] . ' ' . $randomUserData['results'][0]['name']['last'];
            $noticias[$key]->author = $randomAuthor;
        }

    // Paginar manualmente los resultados
        $perPage = 10;
        $currentPageItems = $noticias;
        $noticiasPaginadas = new LengthAwarePaginator($currentPageItems, $totalResults, $perPage, $page);
        $noticiasPaginadas->setPath($request->url());

        return view('noticias.index', compact('noticiasPaginadas'));
    }


}
