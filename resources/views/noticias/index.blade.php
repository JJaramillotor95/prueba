@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        @foreach($noticiasPaginadas as $noticia)
        @if ($noticia->urlToImage)
        <div class="col-md-4 mb-4">
            <div class="card h-100">
                <div style="aspect-ratio: 16/9;">
                    <img src="{{ $noticia->urlToImage }}" class="card-img-top w-100" style="height: 180px;" alt="{{ $noticia->title }}">
                </div>
                <div class="card-body d-flex flex-column">
                    <h5 class="card-title">{{ $noticia->title }}</h5>
                    <p class="card-text">{{ $noticia->description }}</p>
                    <p class="card-text"><small class="text-muted">Publicado el {{ \Carbon\Carbon::parse($noticia->publishedAt)->format('d/m/Y H:i') }}</small></p>
                    <!-- Mostrar el autor -->
                    <p class="card-text"><small class="text-muted">Autor: {{ $noticia->author }}</small></p>
                    <a href="{{ $noticia->url }}" class="btn btn-primary mt-auto">Leer más</a>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
    <div class="row">
       <div class="col-md-12">
        <div class="text-center">
            <p>Página {{ $noticiasPaginadas->currentPage() }} de {{ $noticiasPaginadas->lastPage() }}</p>
        </div>
    </div>
    <div class="col-md-12">
        <div class="d-flex justify-content-center">
            {{ $noticiasPaginadas->links() }}
        </div>
    </div>
</div>
</div>
@endsection
