@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" id="noticias-container">
        <!-- Aquí se cargarán las noticias -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <p>Página <span id="current-page">1</span> de <span id="total-pages">...</span></p>
            </div>
        </div>
        <div class="col-md-12">
            <div class="d-flex justify-content-center">
                <button id="cargar-menos" class="btn btn-primary mr-2">Cargar menos</button>
                <button id="cargar-mas" class="btn btn-primary">Cargar más</button>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        var page = 1;
        var noticiasCargadas = 0;
        var apiKey = '2f49a59ab1cb4c46bdbeaf2a1c5e6db5';
        
        function fetchNoticias(page) {
            var apiUrl = 'https://newsapi.org/v2/top-headlines?country=us&pageSize=10&page=' + page + '&apiKey=' + apiKey;
            
            $.ajax({
                url: apiUrl,
                type: 'GET',
                success: function(response) {
                    var noticias = response.articles;
                    var totalResults = response.totalResults;
                    
                // Actualizar información de paginación
                    $('#current-page').text(page);
                    $('#total-pages').text(Math.ceil(totalResults / 10));
                    
                // Agregar noticias al contenedor
                    noticias.forEach(function(noticia) {
                        if (noticia.urlToImage) {
                        // Llamada a la API randomuser.me para obtener un autor aleatorio
                            $.ajax({
                                url: 'https://randomuser.me/api/',
                                dataType: 'json',
                                success: function(randomUserData) {
                                    var randomAuthor = randomUserData.results[0].name.first + ' ' + randomUserData.results[0].name.last;
                                    
                                    $('#noticias-container').append(`
                                        <div class="col-md-4 mb-4 noticia">
                                        <div class="card h-100">
                                        <div style="aspect-ratio: 16/9;">
                                        <img src="${noticia.urlToImage}" class="card-img-top w-100" style="height: 180px;" alt="${noticia.title}">
                                        </div>
                                        <div class="card-body d-flex flex-column">
                                        <h5 class="card-title">${noticia.title}</h5>
                                        <p class="card-text">${noticia.description}</p>
                                        <p class="card-text"><small class="text-muted">Publicado el ${new Date(noticia.publishedAt).toLocaleString()}</small></p>
                                        <p class="card-text"><small class="text-muted">Autor: ${randomAuthor}</small></p>
                                        <a href="${noticia.url}" class="btn btn-primary mt-auto">Leer más</a>
                                        </div>
                                        </div>
                                        </div>
                                        `);
                                    noticiasCargadas++;
                                }
                            });
                        }
                    });
                    
                // Ocultar el botón "Cargar menos" si no hay noticias cargadas
                    if (noticiasCargadas <= 10) {
                        $('#cargar-menos').hide();
                    } else {
                        $('#cargar-menos').show();
                    }
                    
                // Ocultar el botón "Cargar más" si no hay más noticias para cargar
                    if (page * 10 >= totalResults) {
                        $('#cargar-mas').hide();
                    } else {
                        $('#cargar-mas').show();
                    }
                }
            });
        }
        
    // Cargar noticias al cargar la página
        fetchNoticias(page);
        
    // Manejar clics en el botón "Cargar más"
        $('#cargar-mas').click(function() {
            page++;
            fetchNoticias(page);
        });
        
    // Manejar clics en el botón "Cargar menos"
        $('#cargar-menos').click(function() {
        // Eliminar las últimas 10 noticias cargadas
            $('.noticia').slice(-10).remove();
            noticiasCargadas -= 10;
            
        // Ocultar el botón "Cargar menos" si no quedan noticias para eliminar
            if (noticiasCargadas <= 10) {
                $('#cargar-menos').hide();
            }
            
        // Mostrar el botón "Cargar más"
            $('#cargar-mas').show();
        });
    });
</script>
@endsection
